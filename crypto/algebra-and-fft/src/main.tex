\documentclass{beamer}

\usepackage{algorithm, algorithmic}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[utf8]{inputenc}


\title{Algebra, FFT and cryptography hands-on}
\author{Danny Willems}
\institute{BaDaaS - Nomadic Labs}
\date{Compiled on \today}

\begin{document}

\frame{\titlepage}

\begin{frame}
\tableofcontents
\end{frame}

\section{Algebra}

\subsection{Introduction}

\begin{frame}
  \frametitle{Introduction}
  This presentation will go from abstract theory to practice and real usecases.
  Here we will discuss about
  \begin{itemize}
  \item General results in algebra about structures used in cryptography. Mostly
    on finite structures, even if results can be generalized to some extent to
    arbitrarily infinite sets.
  \item Algebraic results used in cryptographic systems and faster algorithms.
  \item Hands-on with some OCaml implementations of these structures (finite
    fields and polynomials).
  \item Hands-on with some cryptographic systems using these structures and algorithms.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
    \item Mathematics are only about structures over sets (or classes) and
      rules to reason about their elements using formulae over terms (logic). Structures
      with the formulae define how we can \em{model}
        the set (or class). $\rightarrow$ Model Theory
    \item We analyze the different structures over different sets, see the
      transformations between these different structures and try to \em{categorize} them.
      $\rightarrow$ Category Theory
    \item Over the same set, we define different structures (groups, fields,
        rings, modules, polynomials, partial orders, total orders, vectorial spaces,
        topology, elliptic curves, etc).
    \item Between sets with the same structure, we analyze how the elements can
      be transformed from one to the others (morphisms, isomorphisms). It is
      useful when a computation is hard in one structure but easy in an
      isomorphic structure.
  \end{itemize}
\end{frame}

\subsection{Group}
\begin{frame}
  \frametitle{Group - General definition}
  A group is a structure over a set $G$ including a binary internal operation
  ($G \times G \rightarrow G$), noted
  $+_{G}$ or simply $+$ with the following properties:
  \begin{itemize}
    \item $\exists x \in G$ such that $\forall y \in G$, $x + y = y + x = y$. It is
      called the neutral element of $G$.
    \item $\forall x \in G$, $\exists y \in G$ such that $y + x = x + y =
      0_{G}$.
    \item $\forall x, y, z \in G$, $x + (y + z) = (x + y) + z$
  \end{itemize}
  When the operation is denoted by $+$, we say it is an \textbf{additive} group. If the
  operation is denoted $*$ or $.$ we say it is an \textbf{multiplicative} group.
  The neutral element is denoted by $0_{G}$ (resp. $1_{G}$) in the additive
  (resp. multiplicative) representation.
  The structure is sometimes formally denoted by the tuple $(G, +_{G}, 0_{G})$
  (resp. $(G, ._{G}, 1_{G})$). The group is said \textbf{finite} when the set
  $G$ is finite.
\end{frame}

\begin{frame}
  \frametitle{Group - Examples}
  The following sets and associated operations are groups:
  \begin{itemize}
    \item The sets $\mathbb{Z}$, $\mathbb{R}$ and $\mathbb{Q}$ with addition,
      $\mathbb{R}$ or $\mathbb{Q}$ without $0$
      with the product, $\mathbb{C}$.
    \item The permutation of a set $S$ with the composition
    \item The set of continous maps over $\mathbb{R}$ with the addition.
    \item The set ${0, 1, ..., n - 1}$ with the usual addition modulo n. Noted $\mathbb{Z}_{n}$
    \item Matrices $n$ x $m$ with the addition components by components.
    \item The set of $(x, y) \in \mathbb{F}$ (where $\mathbb{F}$ is a field)
      such that $y^{2} = x^{3} + ax + b$, $a$ and $b$ in $\mathbb{F}$ can be
      structured as a group.
  \end{itemize}
  Not a group
  \begin{itemize}
    \item $\mathbb{N}$ with the addition
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Group - general components}
For the rest of the presentation, we will work with \textbf{finite groups}
because they are the only groups interesting in cryptography. The definitions
can be extent to infinite groups, but require rewording. Let's work with a
group $(G, +_{G}, 0_{G})$.
  \begin{itemize}
  \item The order of a finite group $G$ is the cardinal of the underlying set
    $G$. It is often noted $|G|$.
  \item The order of an element $g \in G$ is the smallest $n$ such that
    $\overbrace{g + ... + g}^{n \text{ times}} = n.g = 0_{G}$.
  \item A subgroup of $G$ is a subset $H$ of $G$ such that $(H, +_{G}, 0_{G})$
    is a group.
  \item A group is said \textbf{abelian} or \textbf{commutative} if $\forall x,
    y \in G$, $x + y = y + x$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Group - general components}
  \begin{itemize}
    \item Let $g \in G$. The set $\left\{ \overbrace{g + ... + g}^{n \text{ times}} | n \in \mathbb{N} \right\}$ can
    be structured as a group with the operation $+_{G}$ and $0_{G}$ and is noted
    $\langle {g} \rangle$. The order of the group $\langle {g} \rangle$ is the
    order of $g$, and $\langle {g} \rangle$ is a subgroup of $G$.
    \item An element $g \in G$ is called a \textbf{generator} of $G$ if $\langle
      g \rangle = G$. In other words, any element of $G$ can be expressed using
      $g$.
    \item The group is said \textbf{cyclic} if there is an element $g \in G$ such
      that $\langle g \rangle = G$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Group - General components - Examples}
  Let's take $p = 337$. $\mathbb{Z}_{p}$ is the set $\left\{ 0, 1, ... p - 1
  \right\}$ with the addition modulo $p$.

  \begin{itemize}
  \item The order of the group is $337$
  \item Any non null element is a generator (for instance $85$).
  \item The opposite of $85$ is $252$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Group - Lagrange Theorem}
  \begin{theorem}
    Let $(G, +, 0)$ be a group and $(H, +, 0)$ a subgroup of $G$. Then, $|H|$
    divides $|G|$.
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Group - Lagrange Theorem}
  \begin{theorem}
    Let $(G, +, 0)$ be a group and $(H, +, 0)$ a subgroup of $G$. Then, $|H|$
    divides $|G|$.
  \end{theorem}
  \begin{corollary}
    Let $(G, +, 0)$ a group and $g \in G$. Then the order of $g$ divides $|G|$
  \end{corollary}
\end{frame}

\begin{frame}
  \frametitle{Group - Lagrange Theorem}
  \begin{theorem}
    Let $(G, +, 0)$ be a group and $(H, +, 0)$ a subgroup of $G$. Then, $|H|$
    divides $|G|$.
  \end{theorem}
  \begin{corollary}
    Let $(G, +, 0)$ a group and $g \in G$. Then the order of $g$ divides $|G|$
  \end{corollary}
  \begin{corollary}
    Let $(G, +, 0)$ a group of order $p$ prime. Then any non zero element is a
    generator. In particular, $G$ is cyclic.
  \end{corollary}
\end{frame}

\begin{frame}
  \frametitle{Group - Morphism}
  Note: a group is essentially a certain structure we strengthen on the elements
  of a set. We ask the elements to be linked together with an
  \textit{operation}. For a given set S, we may have plenty of these
  operations. Comes the concept of morphism.
  \begin{definition}
    Let $(G, +_{G}, 0_{G})$ and $(H, +_{H}, 0_{H})$ be groups. Let $f : G
    \rightarrow H$ a function.

    $f$ is called a \textbf{morphism} (or \textbf{homomorphism}) iff
    \begin{itemize}
      \item $f(0_{G}) = 0_{H}$
      \item $\forall a, b \in G$, $f(a +_{G} b) = f(a) +_{H} f(b)$
    \end{itemize}
    If $G$ and $H$ are the same groups, $f$ is called an \textbf{automorphism}.

    If $f$ is bijective, $f$ is called an \textbf{isomorphism}. We say $G$ and
    $H$ are isomorphic and we note $G \cong H$.
  \end{definition}
\end{frame}

\begin{frame}
  \frametitle{Group - Morphism}
  When two groups are isomorphic, it means their structures are essentially the
  same, and computation in one group can be done in the other. It might be
  useful in cryptography when one operation is long to compute in $G$ but fast 
  in $H$.

  The kernel of a morphism is a very important concept. It is the set of
  elements sent by $f$ on the neutral element. It is a subgroup of the group domain.
\end{frame}


\begin{frame}
  \frametitle{Group - Morphism - Example}
  \begin{itemize}
    \item $\mathbb{Z} \rightarrow \mathbb{Z}_{n} : x \rightarrow x \text{ mod }
      n$ is a group morphism.
    \item In ECC, \textit{isogenies} are used. It is a class of
morphisms between elliptic curves preserving algebraic properties and geometric
properties (sic).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Group - Exercices}
  \begin{itemize}
    \item There exists a unique group of order $p$ prime. This group is noted
      $\mathbb{Z}_{p}$ and is the integer addition modulo $p$.
    \item There exists a unique cyclic group of order $n$. It is isomorphic to $\mathbb{Z}_{n}$
    \item A cyclic group is abelian.
    \item Let $n$ be the order of a group $G$. Then, for any divisor $k$ of $n$
      there exists a subgroup $H$ of $G$ of order $k$. It is not necessarily unique.
    \item A subgroup of a cyclic group is cyclic.
    \item $\prod G_{i}$ can be structured as a group by using the $G_{i}$
      operation on the ith component.
  \end{itemize}
\end{frame}

\subsection{Ring}
\begin{frame}
  \frametitle{Ring}
  The idea behind the ring structure is to enforce the links between the
  elements by adding another operation, compatible with the group structure.

  A ring structure on a set $A$ is a tuple $(A, +_{A}, ._{A}, 0_{A}, 1_{A})$ where
  \begin{itemize}
    \item $(A, +_{A}, 0_{A})$ est un groupe
    \item $1_{A} \in A$ and $\forall x \in A$, $x ._{A} 1_{A} = 1_{A} ._{A} x =
      x$ (i.e. $1_{A}$ is a neutral element for $._{A}$)
    \item $\forall x, y, z \in A$, $z ._{A} (x ._{A} y) = (z ._{A} x) ._{A} y$
      (associativity of $._{A}$)
    \item $\forall x, y, z \in A$, $z ._{A} (x +_{A} y) = z ._{A} x +_{A} z ._{A}
      y$ (i.e. left distributivity of $+_{A}$ with $._{A}$)
    \item $\forall x, y, z \in A$, $(x +_{A} y) ._{A} z = x ._{A} z +_{A} y ._{A}
      z$ (i.e. right distributivity of $+_{A}$ with $._{A}$)
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Ring - Notes}
  The definition does not mean there is no inverse for $._{A}$!

  For an element $g$ and an integer $n$, $\overbrace{g . g ... . g}^{\text{n
      times}}$ is noted $g^{n}$.
  The inverse of $g^{n}$ is noted $g^{-n}$.
\end{frame}
\begin{frame}
  \frametitle{Ring - Example}
  \begin{itemize}
    \item $\mathbb{Z}, \mathbb{R}$, $\mathbb{Q}$ with the usual operations.
    \item $\mathbb{Z}_{n}$ with the integer multiplication modulo $n$
    \item The matrices $n \times n$ with the usual addition and multiplication
      on matrices.
    \item A cartesian product of rings is a ring.
  \end{itemize}

  Not a ring
  \begin{itemize}
  \item The matrices $n \times m$, $n \neq m$ with the usual addition and multiplication
    on matrices.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Ring - Morphism}
  The definition of morphism also exists for rings, and extends the same properties
  on the second operation.

  A ring isomorphism is a bijective ring morphism.
\end{frame}

\begin{frame}
  \frametitle{Ring - Chinese Remainder Theorem}
  \begin{theorem}
  Let $n_{1}, ..., n_{k}$ naturals, pairwise coprime (i.e. no common non trivial
  divisors)
and $\mathbb{Z}_{n_{i}}$ the corresponding integer ring. Let $n = \prod_{i} n_{i}$.

  Then $\mathbb{Z}_{n} \cong \prod_{i} \mathbb{Z}_{n_{i}}$ and the map
  $\mathbb{Z}_{n} \rightarrow \prod_{i} \mathbb{Z}_{n_{i}}$
  \begin{equation}
  (x \text{ mod } n) \rightarrow (x \text{ mod } n_{1}, x \text{ mod } n_{2}, ...,  x \text{ mod } n_{k})
  \end{equation}

  is a ring isomorphism. The proof is constructive.
  \end{theorem}

  In particular, if $n = \prod_{i} p_{i}$, $p_{i}$ prime, then $\mathbb{Z}_{n}
  \cong \prod_{i} \mathbb{Z}_{p_{i}}$.
  
  For instance, it shows $\mathbb{Z}_{6} \cong \mathbb{Z}_{3} \times
  \mathbb{Z}_{2}$.

  The theorem also says that if a computation modulo $n$ is hard/takes time, we
  can reduce the problem to modulo $n_{i}$ and get back the solution modulo $n$
  by using the inverse.

  The theorem can also be used for FFT using Good-Thomas algorithm.
\end{frame}

\subsection{Field}
\begin{frame}
  \frametitle{Field}
  Comes one of the most used structure in cryptography : fields. A field is
  simply a ring $(\mathbb{F}, +, ., 0, 1)$ where the non zero elements form a group with the
  multiplication. It means
  \begin{equation}
    \forall x \in \mathbb{F}^{*}, \exists y \in \mathbb{F}, x . y = y . x = 1
  \end{equation}
  and $y$ is unique and is called the inverse of x.

  Examples are $\mathbb{R}$, $\mathbb{C}, \mathbb{Z}_{p}$ (p prime, noted $\mathbb{F}_{p}$)

  The same definition (cyclic, generator, generated group, etc) also holds for
  the multiplicative group.
\end{frame}


\subsubsection{Finite Field}

\begin{frame}
  \frametitle{Finite field}
  Finite fields are the most used fields in cryptography. It can be proved that
  for any $p$ prime and any $n > 0$, there exists a unique field of order
  $p^{n}$, often noted $GF(p^{n})$. For $n = 1$, it is $\mathbb{F}_{p}$, i.e.
  the intergers modulo $p$.
  The order of the multiplicative group is $p - 1$ (which is not prime!).

  For $n > 1$, the field can be constructed by using a quotient of
  $\mathbb{F}_{p}[X]$ by a irreducible polynomial.

  For $p = 2$, the fields $GF(2^{n})$ are sometimes called \textbf{binary
    fields}. These fields are useful to perform computations on data when their
  size in bytes is a power of $2$.

  Note that the structure on $GF(2^{n})$ is more exotic than on $\mathbb{F}_{p}$
\end{frame}

\begin{frame}
  \frametitle{Finite field - Examples}
  Let's take $p = 337$ and $p = 1$. We have then $F_{337}$.

  The multiplicative group of $\mathbb{F}_{337}$ is of order $336$.

  $\left\{ 1, 85, 148, 111, 336, 252, 189, 226 \right\}$ is a subgroup of the
  multiplicative group and is generated by $85$.
  This subgroup is of order $8$.
\end{frame}

\begin{frame}
  \frametitle{Finite field - Properties}
  \begin{itemize}
    \item The multiplicative group of $GF(p^{n})$ is cyclic of order $p^{n} - 1$.
  \end{itemize}
  
\end{frame}



% \subsubsection{Field extension}
% \begin{frame}
%   \frametitle{Field extension}
%   \begin{itemize}
%     \item Closed extension
%     \item can be used to explain the algebraic and geometric properties for
%       the elliptic curves
%   \end{itemize}
% \end{frame}

\subsection{Polynomial ring}
\begin{frame}
  \frametitle{Polynomial ring}
  \begin{itemize}
    \item A polynomial in the variable $X$ is an expression consisting of one
      indeterminate and coefficients. The coefficients are defined over a ring
      $A$. A polynomial $A(X)$ is often given in the form of $\sum_{i \leq n}
      a_{i} X^{i}$.
    \item The $a_{i}$ are called the coefficients
    \item  The set of polynomials over the ring $A$, noted $A[X]$ can be
      structured as a ring using the addition and multiplication over $A$.
    \item The degree $k$ of $A(X)$ is the highest $i$ such that $a_{i}$ is non zero.
    \item A polynomial can be represented in two forms: coefficients form or
      evaluation form (using lagrange interpolation).
    \item In the coefficients forms, $A(X)$ is represented as $(a_{0}, ..., a_{n})$.
    \item In the evaluation form, $A(X)$ is represented by $k + 1$ points $(x_{i},
      y_{i})$ such that $A(x_{i}) = y_{i}$, and the elements of $\left\{ x_{i}
      \right\}$ must be disjoint.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Polynomial interpolation}
  \begin{lemma}[Lagrange interpolation]
    Let $\mathbb{F}$ be a finite field, $\mathbb{F}[X]$ be the polynomials
    over $\mathbb{F}$, $f : \mathbb{F} \rightarrow \mathbb{F}$ a function,
    $(x_{0}, x_{1}, ..., x_{n})$ $n + 1$ points of $\mathbb{F}$ and $(f(x_{0}),
    f(x_{1}), ..., f(x_{n}))$ their evaluations by f. Then, there exists a
    unique polynomial $P(X) \in \mathbb{F}[X]$ of degree \textbf{at most} $n$ such that
    $P(x_{i}) = f(x_{i})$.
  \end{lemma}  
\end{frame}

\begin{frame}
  \frametitle{Polynomial interpolation}
  \begin{lemma}[Lagrange interpolation]
    Let $\mathbb{F}$ be a finite field, $\mathbb{F}[X]$ be the polynomials
    over $\mathbb{F}$, $f : \mathbb{F} \rightarrow \mathbb{F}$ a function,
    $(x_{0}, x_{1}, ..., x_{n})$ $n + 1$ points of $\mathbb{F}$ and $(f(x_{0}),
    f(x_{1}), ..., f(x_{n}))$ their evaluations by f. Then, there exists a
    unique polynomial $P(X) \in \mathbb{F}[X]$ of degree \textbf{at most} $n$ such that
    $P(x_{i}) = f(x_{i})$.
  \end{lemma}  

  It is used in
  \begin{itemize}
    \item numerical analysis to approxiate functions.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Polynomial interpolation}
  \begin{lemma}[Lagrange interpolation]
    Let $\mathbb{F}$ be a finite field, $\mathbb{F}[X]$ be the polynomials
    over $\mathbb{F}$, $f : \mathbb{F} \rightarrow \mathbb{F}$ a function,
    $(x_{0}, x_{1}, ..., x_{n})$ $n + 1$ points of $\mathbb{F}$ and $(f(x_{0}),
    f(x_{1}), ..., f(x_{n}))$ their evaluations by f. Then, there exists a
    unique polynomial $P(X) \in \mathbb{F}[X]$ of degree \textbf{at most} $n$ such that
    $P(x_{i}) = f(x_{i})$.
  \end{lemma}  

  It is used in
  \begin{itemize}
    \item numerical analysis to approxiate functions.
    \item in cryptography...
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Polynomial interpolation}
  The proof is constructive. The idea is to construct a list of $n + 1$ polynomials
  of degree $n$ (called Lagrange basis polynomials) $l_{j}$ such that
  $l_{j}(x_{i}) = \delta_{ij}$. The resulting polynomial will be $\sum_{i}
  f(x_{i})l_{i}$.

  The sum being on polynomials of degree $n$, the resulting polynomial can only be \textbf{at most}
  of degree $n$.
\end{frame}

\begin{frame}
  \frametitle{Polynomial interpolation}
  The proof is constructive. The idea is to construct a list of $n + 1$ polynomials
  of degree $n$ (called Lagrange basis polynomials) $l_{j}$ such that
  $l_{j}(x_{i}) = \delta_{ij}$. The resulting polynomial will be $\sum_{i}
  f(x_{i})l_{i}$.

  The sum being on polynomials of degree $n$, the resulting polynomial can only be \textbf{at most}
  of degree $n$.

  And the basis polynomials are defined as
  \begin{equation}
    l_{j}(X) = \prod_{j \neq k, k = 0}^{n} \frac{X - x_{k}}{x_{j} - x_{k}}
  \end{equation}
\end{frame}


\begin{frame}
  \frametitle{Polynomial interpolation}
  The proof is constructive. The idea is to construct a list of $n + 1$ polynomials
  of degree $n$ (called Lagrange basis polynomials) $l_{j}$ such that
  $l_{j}(x_{i}) = \delta_{ij}$. The resulting polynomial will be $\sum_{i}
  f(x_{i})l_{i}$.

  The sum being on polynomials of degree $n$, the resulting polynomial can only be \textbf{at most}
  of degree $n$.

  And the basis polynomials are defined as
  \begin{equation}
    l_{j}(X) = \prod_{j \neq k, k = 0}^{n} \frac{X - x_{k}}{x_{j} - x_{k}}
  \end{equation}

  The unicity comes from the fact that if two polynomials of at most degree $n$
  agree on $n + 1$ points, then, they are equal everywhere because their
  difference have $n + 1$ roots and then must be zero.
\end{frame}



\begin{frame}
  \frametitle{Polynomial ring - Reducibility}
  A polynome $P(X) \in A[X]$ is said \textbf{reducible} by $Q(X) \in A[X]$ if
  there is $S(X) \in A[X]$ such that $P(X) = Q(X) S(X)$. If $P(X)$ is not
  reducible by any polynomial, it is said \textbf{irreducible}. \textbf{NB:} it
  depends on the underlying ring.

  Examples:
  \begin{itemize}
  \item $X^{2} - X + 1$ is reducible by $X - 1$.
  \item $X^{2} + X + 1$ is reducible in $\mathbb{Z}_{2}$ and
    $\mathbb{C}$ but not in $\mathbb{Q}$ or $\mathbb{R}$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Polynomial ring - Roots (of unity)}
  An element $\omega \in A$ is said to be a root of the polynomial $P(X)$ if
  $P(\omega) = \sum_{i} a_i \omega^{i} = 0_{A}$.

  A \textbf{nth-root of unity} is defined as a root of the polynomial $X^{n} - 1$. In
  other terms, $\omega$ is a nth-root of unity if $\omega^{n} - 1 = 0$ or if
  $\omega^{n} = 1$.

  A nth-root of unity $\omega$, is called \textbf{primitive} if $n$ is the smallest $i$
  such that $\omega^{i} - 1 = 0$.

  Note that if $n$ can be divided by two, then we have
  \begin{itemize}
    \item $\omega$ is a nth-root of unity $\leftrightarrow \omega^{2}$ is a
      $\frac{n}{2}$th root of unity.
    \item $\omega$ is a primitive nth-root of unity $\leftrightarrow \omega^{2}$ is a
      primitive $\frac{n}{2}$th root of unity.
  \end{itemize}

  An additional properties of primitive roots of unity in fields is $\sum_{j
    \leq n - 1} (\omega^{j})^{i} = 0$ for $i \in \left\{ 1, 2, ..., n \right\}$. In
  a ring, a nth root of unity with this property is called
  \textbf{principal}.\footnote{In an integer ring, a primive root of unity is
  always principal}
\end{frame}  

\begin{frame}
  \frametitle{Polynomial ring - Roots (of unity) in finite fields}

  In cryptography, we are only interested in $A = GF(q)$, with $q$ a power of a
  prime number. Any element $x$ of $GF(q)$ is a $q - 1$th root of unity because
  $x^{q - 1} = 1$, the order of $x$ dividing $q - 1$ by the Lagrange theorem,
  and the order of the multiplicative group of $GF(q)$ is $q - 1$.

  If $\omega$ is a nth-root of unity, then $\omega^{i}$ ($i \in \left\{ 0, ...,
    n - 1 \right\}$) is also a nth-root of unity and forms a cyclic group.

  In particular, $\omega$ is
  a primitive dth-root of unity where $d$ is the order of $\omega$ in the
  multiplicative group.

  We also have the following properties if $\omega$ is a primitive nth-root of
  unity:
  \begin{itemize}
    \item $\omega_{n}^{j} \omega_{n}^{k} = \omega_{n}^{(j + k) \text{ mod } n}$
    \item $\omega_{n}^{-1} = \omega_{n}^{n - 1}$
    \item $(\omega_{n}^{k + \frac{n}{2}})^{2} = (\omega_{n} ^ {k})^{2}$ and then
      $-
      \omega_{n}^{k} = \omega_{n}^{k + \frac{n}{2}}$\footnote{This is true in
        integer ring}
  \end{itemize}
\end{frame}

\section{Fast Fourier Transform}

\subsection{Introduction}

\begin{frame}
  \frametitle{Fast/Discret Fourier Transforms (FFT/DFT)}
  For a given field $\mathbb{F}$, its polynomials $\mathbb{F}[X]$, a polynomial
  $P(X) = \sum_{j \leq n - 1} a_{j} X^{j} \in \mathbb{F}[X]$ of degree $n - 1$ and
  $\omega_{n}$ a generator of the primitive n-th roots of unity, computing
  \begin{equation}
    f_{k} = \sum_{j \leq n - 1} a_{j}(\omega_{n}^{k})^{j}
  \end{equation}
  The naive algorithm will be in $O(n^{2})$.
  
  Some vocabulary (from signal processing):
  \begin{itemize}
    \item $j$ is called the \textbf{time}.
    \item $a_{j}$ is called the \textbf{time domain}.
    \item $k$ is called the \textbf{frequency}.
    \item $f_{k}$ is said to be in the \textbf{frequency domain} and called
      \textbf{spectrum} of $a_{j}$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Fast/Discret Fourier Transforms (FFT/DFT)}
  \begin{itemize}
    \item General idea: use some properties of the underlying scalar structure
      to speed up some algorithms. \textbf{It means it can not always be applied!}.
    \item Useful to fasten scalar multiplication, n-degree polynomial multiplications
      and polynomial interpolation.
    \item In the algorithm given below, it is supposed $n$ is a power of 2. It
      is called the Cooley-Tukey FFT algorithm. Variants exist. The algorithm
      will be in $O(n log(n))$. It divides the n-degree polynomial $P(X)$ in two
      $n / 2$-degree polynomials $P_{0}(X)$ and $P_{1}(X)$ keeping the same
      hypothesis on the evaluation domain. The algorithm can be then repeated on
      $P_{0}(X)$ and $P_{1}(X)$.
  \end{itemize}
\end{frame}

\subsection{Cooley-Tukey FFT}

\begin{frame}
  \frametitle{Cooley-Tukey FFT}
  The polynomial $P(X) = a_{0} + a_{1} X + ... + a_{n - 1} X^{n - 1}$ is
splitted in polynomial $P_{even}(X)$ and $P_{odd}(X)$ in the following way:

  \begin{equation}
    P_{even}(X) = a_{0} + a_{2} X + ... + a_{n - 2} X^{n / 2 - 1}
  \end{equation}
  \begin{equation}
    P_{odd}(X) = a_{1} + a_{3} X + ... + a_{n - 1} X^{n / 2 - 1}
  \end{equation}

  and we have
  \begin{equation}
    P(X) = P_{even}(X^{2}) + X . P_{odd}(X^{2})
  \end{equation}

  The polynomial $P_{even}$ and $P_{odd}$ can be used in a recursive call to the
  algorithm with the new points $\omega_{n}^{2i} = \omega_{\frac{n}{2}}^{i}$.
  The conditions of the algorithm are still true on $P_{even}$ and $P_{odd}$ and
  also on the domain ($\omega^{i}_{\frac{n}{2}}$)

  See ocaml-polynomial for the implementation and examples.
\end{frame}

\begin{frame}
  \frametitle{Cooley-Tukey FFT - Algorithm}
  Input:
  \begin{itemize}
  \item $\omega_{n}$, a $n$-th root of unity. The domain $\left\{
      \omega_{n}^{i} \right\}$ can be pre-computed.
  \item $a_{j}$, the coefficients of
    the polynomial $P$ of degree $n - 1$
  \item $n$ must be a power of $2$.
  \end{itemize}

  Output:
  \begin{itemize}
      \item $P(1)$, $P(\omega_{n})$, $P(\omega_{n}^{2})$, $P(\omega_{n}^{3})$,
        ..., $P(\omega_{n}^{n - 1})$, i.e. the evaluation of $P$ in each $n$-th
        root of unity.
  \end{itemize}
  Complexity in O(n log(n)) (instead of O($n^{2}$))

  Let's remark
  \begin{align}
    P(\omega_{n}^{i + \frac{n}{2}}) & = P_{even}((\omega_{n}^{i + \frac{n}{2}})^{2}) + \omega_{n}^{i + \frac{n}{2}} P_{odd}((\omega_{n}^{i + \frac{n}{2}})^{2}) \\
                               & = P_{even}((\omega_{n}^{i})^{2}) + \omega_{n}^{i + \frac{n}{2}} P_{odd}((\omega_{n}^{i})^{2}) \\
                               & = P_{even}((\omega_{n}^{i})^{2}) - \omega_{n}^{i} P_{odd}((\omega_{n}^{i})^{2})
  \end{align}
\end{frame}

\begin{frame}
  \frametitle{Cooley-Tukey FFT - Algorithm - Pseudocode}
  \begin{algorithm}[H]
    \caption{Cooley Tukey FFT}
    \begin{algorithmic}[1]
      \IF{$length(coefficients) = 1$}
        \RETURN coefficients
      \ENDIF
        \STATE $newDomain \gets [(\omega_{n}^{i}) ^{2}]$
        \STATE $evenCoefficients \gets [a_{2i}]$
        \STATE $FFTEven \gets$ \text{CooleyTukey}($newDomain$, $evenCoefficients$,
        $\frac{n}{2}$)
        \STATE $oddCoefficients \gets [a_{2i + 1}]$
        \STATE $FFTOdd \gets$ \text{CooleyTukey}($newDomain$, $oddCoefficients$,
        $\frac{n}{2}$)
        \STATE $output \gets$ empty list of length $n$
        \FOR{$i = 0$ to $\frac{n}{2}$}
          \STATE $output[i] = FFTEven[i] + domain[i] * FFTOdd[i]$
          \STATE $output[i + \frac{n}{2}] = FFTEven[i] - domain[i] * FFTOdd[i]$
        \ENDFOR
        \RETURN output
    \end{algorithmic}
  \end{algorithm}
\end{frame}

\subsection{Inverse FFT}

\begin{frame}
  \frametitle{Inverse FFT}
  Previously, from $P(X) = \sum_{i \leq n - 1} a_{i} X^{i}$, we wanted to compute
  \begin{equation}
    P(\omega_{n}^{k}) = f_{k} = \sum_{j \leq n - 1} a_{j}(\omega_{n}^{k})^{j}
  \end{equation}
  i.e. evaluating the polynomial in $\omega_{n}^{k}$.

  The inverse FFT problem is to compute the polynomial coefficients $a_{j}$ given
  $f_{k}$.
  
  The equation is given by
  \begin{equation}
    a_{j} = \frac{1}{n} \sum_{k \leq n - 1} f_{k} (\omega_{n}^{-j})^{k}
  \end{equation}

  The equality can be proved by using the fact $\omega_{n}$ is a
  \textit{principal} root of unity and the equality
  \begin{equation}
    \sum_{j \leq n - 1} (\omega^{j})^{i} = 0
  \end{equation} stands for any $i$
\end{frame}

\begin{frame}
  \frametitle{Inverse FFT}
  Given the equation
  \begin{equation}
    a_{j} = \frac{1}{n} \sum_{k \leq n - 1} f_{k} (\omega_{n}^{-j})^{k}
  \end{equation}

  we notice it is exactly the FFT problem, with the time domain $f_{k}$ and the
  roots of unity generated by $\omega_{n}^{-1}$

  See ocaml-polynomial for the implementation and examples.
\end{frame}

\begin{frame}
  \frametitle{Cooley-Tukey FFT - Algorithm}
  Input:
  \begin{itemize}
  \item $\omega_{n}^{-1}$, the inverse of an $n$-th root of unity.
  \item $f_{k}$, the evaluation of the polynomial $P$ at the points $\omega_{n}^{k}$
  \item $n$ must be a power of $2$.
  \end{itemize}

  Output:
  \begin{itemize}
      \item $a_{0}$, $a_{1}$, ..., $a_{n - 1}$ i.e. the coefficients of $P$.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Cooley-Tukey Inverse FFT - Algorithm - Pseudocode}
  \begin{algorithm}[H]
    \caption{Cooley Tukey Inverse FFT}
    \begin{algorithmic}[1]
      \STATE $inverseDomain \gets [(\omega_{n}^{-i})]$
      \STATE $coefficients \gets CooleyTukey(inverseDomain, [f_{k}], n)$
      \RETURN $\frac{1}{n} coefficients$
    \end{algorithmic}
  \end{algorithm}
\end{frame}

\subsection{Polynomial multiplication}

\begin{frame}
  \frametitle{FFT - Polynomial multiplication}
  FFT algorithms can also be used to optimize polynomial multiplication computation.
  Given two polynomials $P(X)$ and $Q(X)$, the problem is to compute efficiently the
  polynomial
  \begin{align}
    (P . Q) (X) & = (\sum_{i \leq n} a_{i} X^{i}) (\sum_{j \leq n} b_{j} X^{j}) \\
                & = \sum_{i \leq 2n} \sum_{j \leq i} a_{i} b_{i - j} X^{i}
  \end{align}

  The naive algorithm is in $O(n^{2})$. Using FFT, it can be computed in $O(n log(n))$.
\end{frame}

\begin{frame}
  \frametitle{FFT - Polynomial multiplication}
  The idea:
  \begin{itemize}
    \item Remember $2n$ points can define a polynomial of degree $2n - 1$.
    \item Then, if we have $2n$ evaluated points of $P(X) . Q(X)$, we
      can interpolate it in O(n log n).
      \item For that, we can evalute $P(X)$ and $Q(X)$ seperately in $2n$
        points, and multiply the result. However, that would take $O(n^{2})$
      \item Instead, we evaluate $P'(X) = X^{n} P(X)$ (resp $Q'(X) = X^{n} Q(X)$)
        in $\omega_{2n}^{i}$ which is in O(n log(n)), multiply the results point
        wise (in O(n)), and interpolate
        the polynomial $P(X).Q(X)$ using the $2n$ points (in O(n log(n))).
  \end{itemize}
\end{frame}


\section{Cryptographic protocols and primitives in action}

\subsection{ocaml-ff}
\begin{frame}
  \frametitle{OCaml FF - Play with finite fields in OCaml}
  \begin{itemize}
    \item Source: https://gitlab.com/dannywillems/ocaml-ff
    \item Idea: OCaml implementation of finite fields of any order
    \item Use \textit{libgmp} (by using \textit{zarith}) to have a fast
      implementation on most platforms.
    \item Transpiles to JavaScript easily for a web usage using
      \textit{js\_of\_ocaml} and \textit{zarith\_stubs\_js} (JaneStreet).
    \item Easy and simple instanciation of a finite field: using functor
    \item Using OCaml module sytsem, easy to overwrite some functions with faster
      implementation for a given order or usecase.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{OCaml FF - Play with finite fields in OCaml}
  \begin{itemize}
    \item For the moment, only $\mathbb{F}_{p}$ with $p$ prime is implemented
      (the easy part). $\mathbb{F}_{p^{n}}$ requires a more exotic structure.
      Coming soon.
    \item Provides a functorized set of tests to verify the implementation by
      testing mathematical fields properties.
    \item Hands-on
  \end{itemize}
\end{frame}


\subsection{ocaml-polynomial}
\begin{frame}
  \frametitle{OCaml Polynomial - Play with polynomials in OCaml}
\end{frame}

% \subsection{ocaml-pallier}

% \begin{frame}
%   \begin{itemize}
%     \item Implementation of the Pallier cryptosystem using Zarith.
%   \end{itemize}
% \end{frame}

\subsection{ocaml-shamir}

\begin{frame}
  \frametitle{OCaml Shamir - Implementation}
  \begin{itemize}
    \item Implementation of the Shamir secret sharing protocol.
    \item Source: https://gitlab.com/dannywillems/ocaml-shamir
    \item Functor in mind: can be instanciated with any field.
    \item JavaScript interoperability: can be transpiled to JavaScript easily to
      use in a web app.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Shamir Shamir Secret Sharing protocol}
  The idea is to hide a secret $S$ between $n$ participants and requiring $d$
  persons to unlock the secret. It is based on polynomials and the concept of
  polynomial interpolation. $S$ is going to be hidden in a polynomial of degree
  $d - 1$ (for example, it can be the highest coefficient), and points of this
  polynomial will be sent to the participants. As the degree is $d - 1$, $d$
  evaluation points are enough to reconstruct the polynomial and find the secret.
  % \begin{itemize}
  %   \item Generate 
  % \end{itemize}
\end{frame}

\end{document}
